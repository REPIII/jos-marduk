
#include "fs.h"

// IMPORTANT EXTERNAL DESCRIPTIONS //
//
// BLKSECTS is defined in fs/fs.h
// ide_read is defined in fs/ide.c
// ide_write is defined in fs/ide.c
// PGNUM is defined in inc/mmu.h
// ROUNDDOWN is defined in inc/types.h
// uvpt[] is defind in inc/memlayout.h
// va_is_dirty is defined HERE.
// va_is_mapped is defined HERE.
//
// IMPORTANT EXTERNAL DESCRIPTIONS //

// Return the virtual address of this disk block.
void*
diskaddr(uint64_t blockno)
{
	if (blockno == 0 || (super && blockno >= super->s_nblocks))
		panic("bad block number %08x in diskaddr", blockno);
	return (char*) (DISKMAP + blockno * BLKSIZE);
}

// Is this virtual address mapped?
bool
va_is_mapped(void *va)
{
	return (uvpml4e[VPML4E(va)] & PTE_P) && (uvpde[VPDPE(va)] & PTE_P) && (uvpd[VPD(va)] & PTE_P) && (uvpt[PGNUM(va)] & PTE_P);
}

// Is this virtual address dirty?
bool
va_is_dirty(void *va)
{
	return (uvpt[PGNUM(va)] & PTE_D) != 0;
}

// Fault any disk block that is read in to memory by
// loading it from disk.
// Hint: Use ide_read and BLKSECTS.
static void
bc_pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint64_t blockno = ((uint64_t)addr - DISKMAP) / BLKSIZE;
	int r;

	// Check that the fault was within the block cache region
	if (addr < (void*)DISKMAP || addr >= (void*)(DISKMAP + DISKSIZE))
		panic("page fault in FS: eip %08x, va %08x, err %04x",
		      utf->utf_rip, addr, utf->utf_err);

	// Sanity check the block number.
	if (super && blockno >= super->s_nblocks)
		panic("reading non-existent block %08x\n", blockno);

	// Allocate a page in the disk map region, read the contents
	// of the block from the disk into that page.
	// Hint: first round addr to page boundary.
	//
	// LAB 5: your code here:

	// Page align addr and allocate the page.
	addr  = ROUNDDOWN(addr, PGSIZE);
	r = sys_page_alloc(
		0, addr, PTE_P | PTE_U | PTE_W
	);
	if (r < 0) {
		panic(
			"bc_pgfault panic: Could not allocate page for disk" \
			"read.\nError code from sys_page_alloc: %e", r
		);
	}
	// ide_read reads in units of sectors, not blocks.
	r = ide_read(blockno * BLKSECTS, addr, BLKSECTS);
	if (r < 0) {
		panic(
			"bc_pgfault panic: Could not read from disk.\nError " \
			"code from ide_read: %e", r
		);
	}

	// Check that the block we read was allocated. (exercise for
	// the reader: why do we do this *after* reading the block
	// in?)
	if (bitmap && block_is_free(blockno))
		panic("reading free block %08x\n", blockno);

}

// Flush the contents of the block containing VA out to disk if
// necessary, then clear the PTE_D bit using sys_page_map.
// If the block is not in the block cache or is not dirty, does
// nothing.
// Hint: Use va_is_mapped, va_is_dirty, and ide_write.
// Hint: Use the PTE_SYSCALL constant when calling sys_page_map.
// Hint: Don't forget to round addr down.
void
flush_block(void *addr)
{
	uint64_t blockno = ((uint64_t)addr - DISKMAP) / BLKSIZE;
	int rc = 0;

	if (addr < (void*)DISKMAP || addr >= (void*)(DISKMAP + DISKSIZE))
		panic("flush_block of bad va %08x", addr);

	// LAB 5: Your code here.

	void *pg_bottom = ROUNDDOWN(addr, PGSIZE);
	// is page isn't dirty or mapped, we silently do nothing.
	if (!va_is_dirty(pg_bottom) || !va_is_mapped(pg_bottom)) {
		return;
	}

	rc = ide_write(blockno * BLKSECTS, pg_bottom, BLKSECTS);
	if (rc < 0) {
		panic(
			"flush_block panic: Could not write to disk.\nError " \
			"code from ide_write: %e", rc
		);
	}
	// 0 is an allias for curenv->envid.
	rc = sys_page_map(
		0, pg_bottom, 0, pg_bottom, PTE_SYSCALL
	);
	if (rc < 0) {
		panic(
			"flush_block panic: Could not re-map map at same " \
			"location\nin the same environment in order to " \
			"reset PTE_D bit.\nError code from sys_page_map: " \
			"%e", rc
		);
	}

}

// Test that the block cache works, by smashing the superblock and
// reading it back.
static void
check_bc(void)
{
	struct Super backup;

	// back up super block
	memmove(&backup, diskaddr(1), sizeof backup);

	// smash it
	strcpy(diskaddr(1), "OOPS!\n");
	flush_block(diskaddr(1));
	assert(va_is_mapped(diskaddr(1)));
	assert(!va_is_dirty(diskaddr(1)));

	// clear it out
	sys_page_unmap(0, diskaddr(1));
	assert(!va_is_mapped(diskaddr(1)));

	// read it back in
	assert(strcmp(diskaddr(1), "OOPS!\n") == 0);

	// fix it
	memmove(diskaddr(1), &backup, sizeof backup);
	flush_block(diskaddr(1));

	cprintf("block cache is good\n");
}

void
bc_init(void)
{
	struct Super super;
	set_pgfault_handler(bc_pgfault);
	check_bc();

	// cache the super block by reading it once
	memmove(&super, diskaddr(1), sizeof super);
}

