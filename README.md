# **JOS Marduk**

JOS Marduk is a exokernel for educational purposes, targeting x86.

JOS Markduk supports a simple shell, file system, and simple user-level
programs in a multi-process environment. It is run on
[QEMU.](https://www.qemu.org/)

## Building JOS Markduk
-----

JOS Markduk cannot be built with standard GNU tools, which, as of this
writing, are GCC 8.3.0-6 and Bintutils 2.31.1. GCC produces too large of a
binary to fit in the boot block, and ld does not link all assembly-defined
globals correctly. The reason behind the latter I have not able to fully
determine.

The latest versions of dependent software known to work with JOS Marduk are
GCC 4.6.4 (patched...more on that in a moment), GNU Binutils 2.21.1, and
QEMU 1.7.0. QEMU 1.7.0 is simply the version distributed, as of this writing,
by Debian 10. Therefore, I will not detail how to build QEMU here. (I have never
built QEMU myself.)

Since the steps for installing this software correctly was not a simple task
for myself, I had never attempted to build GCC or Binutils before, I will
detail the steps here.

### Building [Binutils,](https://www.gnu.org/software/binutils/) [2.21.1](http://ftpmirror.gnu.org/binutils/binutils-2.21.1.tar.bz2)
-----

Building binutils requires the following configurations:

    ./configure --prefix=/usr/local --target=x86_64-jos-elf --disable-werror
    make
    make install

binutils is installed with the prefix "x86\_64-jos-elf". This is to
distinguish it from the standard binutils installed on the system, and any
other binutils versions that may be on the system.

JOS as distributed is flexible in its search for a linker. I found that
flexibility to be a downside, and so now JOS's makefile looks for
"x86_64-jos-elf-ld" specifically. This can be reconfigured through JOS's
makefile.

### Building [GCC,](http://gcc.gnu.org/) [4.6.4](http://ftpmirror.gnu.org/gcc/gcc-4.6.4/gcc-core-4.6.4.tar.bz2)
-----

GCC 8.3.0-6 cannot build GCC 4.6.4 without first
[patching 4.6.4.](https://web.archive.org/web/20200628205216/https://gcc.gnu.org/legacy-ml/gcc-patches/2009-06/msg01249.html)

GCC itself is dependent on GMP, MPFR, and MPC.
[GMP,](https://gmplib.org/)
[version 5.0.2,](ftp://ftp.gmplib.org/pub/gmp-5.0.2/gmp-5.0.2.tar.bz2)
[MPFR,](https://www.mpfr.org/)
[version 3.1.2,](https://web.archive.org/web/20171020085250/https://www.mpfr.org/mpfr-3.1.2/mpfr-3.1.2.tar.bz2)
and [MPC,](http://www.multiprecision.org/mpc)
[version 0.9,](https://web.archive.org/web/20180624124743/http://www.multiprecision.org/downloads/mpc-0.9.tar.gz)
are known to work.

Building GMP, MPFR, and MPC do not require any unique configurations. Building
is as straightforward as calling `make`. These are the commands I used, but
you can use others:

    ./configure --prefix=/usr/local
    make
    make install

These are the commands I used to build GCC:

    ../configure --prefix=/usr/local \
        --target=x86_64-jos-elf --disable-werror \
        --disable-libssp --disable-libmudflap --with-newlib \
        --without-headers --enable-languages=c MAKEINFO=missing
    make all-gcc
    make install-gcc
    make all-target-libgcc
    make install-target-libgcc

GCC is installed with the prefix "x86\_64-jos-elf". This is to distinguish it
from the standard GCC installed on the system, and any other GCC versions
that may be on the system.

JOS as distributed is flexible in its search for a C compiler. I found that
flexibility to be a downside, and so now JOS's makefile looks for
"x86_64-jos-elf-gcc" specifically. This can be reconfigured through JOS's makefile.

### A Note on GDB
-----

When I was developing JOS as a part of my OS course, I was told that GDB needed to
be patched. I have not found this to be case. GDB 8.2.1, what is distributed by
Debian 10 as of this writing, works without issue. However, I will link the
[patch](https://web.archive.org/web/20200801214128/https://www.cs.unc.edu/~porter/courses/cse591/s14/gdb-7.2-arch-remote-change.patch)
in case anyone else finds issues with GDB.

## Running JOS-Markduk
-----

JOS is straightforward to build and run using make.

To build, simply use `make`. To run on QEMU, use `make qemu`. However, since
there's no graphical support besides a terminal at the moment, the most
common invocation is `make qemu-nox`. This will run QEMU in `-nographics` mode
directly within the invoking terminal. Running this will complete a partial
build of JOS and run JOS, bringing the user into JOS's terminal following a
successful boot. From here one can run JOS's selection of installed binaries.
To shut down QEMU from `-nographics mode`, use CTRL+A+X.

To debug, `make qemu-nox-gdb` is the standard invocation.

There are a few other make invocations available for other jobs. Consult make for more information.

## Is JOS Markduk any different from [MIT 6.828's JOS?](https://web.archive.org/web/20190713111904/https://pdos.csail.mit.edu/6.828/2018/labguide.html)
-----

Not really, no.

When we set up our projects we were asked to name our project, and I named
mine Markduk. Since I intend to go beyond what I was asked to do for JOS in
my OS course, I felt it was appropriate to keep the name. I hope that one day
it evolves beyond JOS, even if only by a little bit in the grand scheme of
things.

## License
-----

JOS Markduk is *mostly* copyrighted by MIT under an MIT license, which can be
found at LICENSE.txt. That license applies to all files that are not described
by inc/COPYRIGHT and kern/COPYRIGHT, which apply to those respective
directories and have further information. When not otherwise specified, a file
in question is copyrighted by MIT under LICENSE.txt.

kern/multiboot.h is copyrighted by Free Software Foundation, Inc. under a GNU
General Public License, version 2. More information can be found in
kern/multiboot.


## Future
-----

The intention is that JOS Marduk is not complete. I intend to add a hypervisor,
and add the VGA and syscall support necessary to run DOOM. At that point
I believe I will be content that JOS Markduk is complete.

Why DOOM? Getting JOS to run DOOM is the most impressive project
Professor Porter has seen done in JOS, completed by a group when he
was a TA at Texas. So why not? Sounds fun to me!

That being said, work on JOS Markduk is not active. I do not expect
activity to pick back up again until 2023 when I have graduated from
graduate school. At best, JOS Markduk is dormant. At worst, it's dead.

## Acknowledgements
-----


This would not have been possible without the starting coding provided by
[MIT](https://pdos.csail.mit.edu/6.828/2018/labguide.html)
and everyone there who made it possible. Writing an OS from scratch over a
semester, or really ever, is overwhelming for a novice. The starting skeleton
provided was critical in giving myself the confidence I could proceed and
succeed. I suggest everyone start with some skeleton code if they want to
get their toes wet into the world of OS development, and then grow from there.

I also would like to thank [Professor Porter](https://www.cs.unc.edu/~porter/)
for challenging a bunch of undergraduates to tackle something like JOS.
Giving students difficult work, and allowing some to fail while others learn
a tremendous amount, is a philosophy that is fading in undergraduate academia.
It was absent when I TA'd the course a year later under a different professor.
Those students learned not even a tenth of what I learned through JOS.

I am very lucky to have taken OS when I did, to be admitted into the
Honors section, and to had have this opportunity.

For that, I am forever grateful.
