// User-level IPC library routines

#include <inc/lib.h>

// IMPORTANT EXTERNAL DESCRIPTIONS //
//
// Env is defined in inc/env.h
// UTOP is defined in inc/memlayout.h
//
// All error codes are defined in inc/error.c
//
// IMPORTANT EXTERNAL DESCRIPTIONS //

// Receive a value via IPC and return it.
// If 'pg' is nonnull, then any page sent by the sender will be mapped at
//	that address.
// If 'from_env_store' is nonnull, then store the IPC sender's envid in
//	*from_env_store.
// If 'perm_store' is nonnull, then store the IPC sender's page permission
//	in *perm_store (this is nonzero iff a page was successfully
//	transferred to 'pg').
// If the system call fails, then store 0 in *fromenv and *perm (if
//	they're nonnull) and return the error.
// Otherwise, return the value sent by the sender
//
// Hint:
//   Use 'thisenv' to discover the value and who sent it.
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{


	// "Illegal" pg is at or above UTOP according to sys_ipc_recv,
	// but an "illegal" pg for us is NULL. Thus, we have to change
	// pg if appropriate.
	// Otherwise sys_ipc_recv does all the checks, and we manage its
	// returns.

	int rc;

	if (!pg) {
		// This is 0xFF...FF. I like it more than UTOP because it
		// isn't page alligned.
		rc = sys_ipc_recv((void *)-1);
	} else {
		rc = sys_ipc_recv(pg);
	}

	// Set up our return values depended on whether or not we
	// received a message.
	if (rc < 0) {

		if (from_env_store) {
			from_env_store = 0;
		}
		if (perm_store) {
			*perm_store = 0;
		}

		return rc;

	} else {

		if (from_env_store) {
			*from_env_store = (thisenv)->env_ipc_from;
		}
		if (perm_store) {
			*perm_store = (thisenv)->env_ipc_perm;
		}

		return (thisenv)->env_ipc_value;

	}

}

// Send 'val' (and 'pg' with 'perm', if 'pg' is nonnull) to 'toenv'.
// This function keeps trying until it succeeds.
// It should panic() on any error other than -E_IPC_NOT_RECV.
//
// Hint:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{

	// "Illegal" pg is at or above UTOP according to sys_ipc_recv,
	// but an "illegal" pg for us is NULL. Thus, we have to change
	// pg if appropriate.
	// Otherwise sys_ipc_send does all the checks.

	int rc;

	if (!pg) {
		// This is 0xFF...FF. I like it more than UTOP because it
		// isn't page alligned.
		pg = (void *)-1;
	}

	do {
		rc = sys_ipc_try_send(to_env, val, pg, perm);
		sys_yield();
	} while (rc == -E_IPC_NOT_RECV);

	// We need to check why we exited. It it's not E_IPC_NOT_RECV,
	// we panic.
	if (rc != 0) {
		panic(
			"ipc_send panic: sys_ipc_try_send failed to send.\n" \
			"Error code: %e", rc
		);
	}

}


// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++) {
		if (envs[i].env_type == type)
			return envs[i].env_id;
	}
	return 0;
}
