// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// IMPORTANT EXTERNAL DESCRIPTIONS //
//
// envs is defined in kern/env.c
// ENVX is defined in inc/env.h
// FEC_U is defined in inc/mmu.h
// FEC_WR is defined in inc/mmu.h
// memcpy is defined in lib/string.c
// PTTEMP is defined in inc/memlayout.h
// PTE_COW is defined here in lib/fork.c NOT INC/MMU.H
// PTE_SHARE is defined in inc/lib.h
// set_pgfault is defined in lib/pgfault.c
// thisenv is defined in inc/lib.h
// UTrapframe is defined inc/trap.h
//
// All virtual address array indices (like VPN) are defined in
// inc/mmu.h
//
// All PTE bits (EXCEPT PTE_COW) are defined in inc/mmu.h
//
// All virtual address arrays (like uvpt[]) are defined
// in inc/memlayout.h
//
// All functions that lead with "sys_" are defined
// in kern/syscall.c
//
// IMPORTANT EXTERNAL DESCRIPTIONS //

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes
// (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	// @REPIII@ I never saw the need for err so I commented it out.
	//uint32_t err = utf->utf_err;
	int rc = 0;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page. If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).
	//
	// LAB 4: Your code here.

	// Check if the error was from a write in user space.
	if ((utf->utf_err & (FEC_WR | FEC_U)) != (FEC_WR | FEC_U)) {
		panic(
			"pgfault panic: Fault was not caused by a write in\n" \
			"user space. Error code (4 is user read): %d\nFauling va: %p\n",
			utf->utf_err, addr
		);

	// Ensure that the page table in which the fault occur is
	// copy-on-write. Otherwise this was a true page fault requiring
	// environment destruction.
	// There is no need to check tables higher than the page table
	// index.
	} else if ((uvpt[VPN(addr)] & PTE_COW) != PTE_COW) {
		panic("pgfault panic: pte is not copy-on-write");
	}

	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then
	// move the new page to the old page's address.
	// Hint:
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.
	//
	// LAB 4: Your code here.

	// Zero is an alias for curenv->env_id
	rc = sys_page_alloc(0, PFTEMP, PTE_P | PTE_U | PTE_W);
	if (rc < 0) {
		panic(
			"pgfault panic: Could not allocate page at PFTEMP\n" \
			"Error code from sys_page_alloc: %e", rc
		);
	}
	// memmove has a return, but it is not an error indication.
	memmove(PFTEMP, ROUNDDOWN(addr, PGSIZE), PGSIZE);
	// Zero is an alias for curenv->env_id
	rc = sys_page_map(
		0,
		PFTEMP,
		0,
		ROUNDDOWN(addr, PGSIZE),
		PTE_P | PTE_U | PTE_W
	);
	if (rc < 0) {
		panic(
			"pgfault panic: Could not map page from PFTEMP to " \
			"%p\nError code from sys_page_map: %e",
			addr, rc
		);
	}
	// Zero is an alias for curenv->env_id
	rc = sys_page_unmap(0, PFTEMP);
	if (rc < 0) {
		panic(
			"pgfault panic: Could not unmap PFTEMP. Error code " \
			"from\nsys_page_unmap: %e", rc
		);
	}

}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address. If the page is writable or
// copy-on-write, the new mapping must be created copy-on-write,
// and then our mapping must be marked copy-on-write as well.
// (Exercise: Why do we need to mark ours copy-on-write again if it
// was already copy-on-write at the beginning of this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{

	void *va = (void *)((uint64_t)pn << PTXSHIFT);

	// If the page is not present, immediately return.
	// We can't copy a page that isn't present, so this is
	// technically a success. Zero is the proper return value.
	if (
		((uvpml4e[VPML4E(va)] & PTE_P) != PTE_P)
		|| ((uvpde[VPDPE(va)] & PTE_P) != PTE_P)
		|| ((uvpd[VPD(va)] & PTE_P) != PTE_P)
		|| ((uvpt[VPN(va)] & PTE_P) != PTE_P)
	) {
		return 0;
	}

	int rc = 0;

	//cprintf("duppage va: %p PERM %x\n", va, uvpt[pn] & PTE_SYSCALL);

	// If the page is shared or does not have write permissions, map
	// the map into the child environment directory. Don't worry
	// about PTE_COW bit.
	if (((uvpt[pn] & PTE_SHARE) == 0)
		&& (
			((uvpt[pn] & PTE_W) == PTE_W)
			|| ((uvpt[pn] & PTE_COW) == PTE_COW)
		)
	) {

		// perm variable is to make the code more legible.
		int perm = ((uvpt[pn] & ~(PTE_W)) | PTE_COW) & PTE_SYSCALL;
		//cprintf("perm: %x\n", perm);
		rc = sys_page_map(0, va, envid, va, perm);
		if (rc < 0) {
			return rc;
		}
		rc = sys_page_map(0, va, 0, va, perm);

	} else {

		//cprintf("duppage not COW VA: %p PERM: %x\n", va, uvpt[pn] & PTE_SYSCALL);
		//cprintf("perm %x\n", uvpt[pn] & PTE_SYSCALL);
		rc = sys_page_map(0, va, envid, va, uvpt[pn] & PTE_SYSCALL);

	}

	return rc;

}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{

	// Setup the pgfault_handler before anything else.
	// It has no failure indication. It panics.
	set_pgfault_handler(pgfault);

	envid_t envid_child = sys_exofork();
	if (envid_child < 0) {
		return envid_child;
	} else if (envid_child == 0) {
		// We're the child.
		thisenv = &envs[ENVX(sys_getenvid())];
		return 0;
	}

	// We're the parent, and we now have to set everything up.

	int rc = 0;

	// Duplicate all user pages using duppage so COW bit is set.
	// Only duplicate if present. duppage checks for present bit so
	// we don't have to.
	uintptr_t va;
	for (va = UTEXT; va < USTACKTOP; va += PGSIZE) {
		// duppage wants page number, not address.
		rc = duppage(envid_child, PPN(va));
		if (rc < 0) {
			return rc;
		}
	}

	// Allocate and map child's UX stack.
	//
	// Allocate a new page for the child's UX stack.
	rc = sys_page_alloc(envid_child, (void *)(UXSTACKTOP - PGSIZE), PTE_P | PTE_U | PTE_W);
	if (rc < 0) {
		return rc;
	}
	// Map child's UX stack to parent's PFTEMP.
	rc = sys_page_map(
		envid_child,
		(void *)(UXSTACKTOP - PGSIZE),
		0,
		UTEMP,
		PTE_P | PTE_U | PTE_W
	);
	if (rc < 0) {
		return rc;
	}
	// Copy the parent's UX stack to parent's PFTEMP.
	// memmove has a return, but it is not an error indication.
	memmove(UTEMP, (void *)(UXSTACKTOP - PGSIZE), PGSIZE);
	// Unmap parent's mapping at PFTEMP.
	rc = sys_page_unmap(0, UTEMP);
	if (rc < 0) {
		return rc;
	}

	// Give our page fault handler to the child.
	rc = sys_env_set_pgfault_upcall(
		envid_child,
		(thisenv)->env_pgfault_upcall
	);
	if (rc < 0) {
		return rc;
	}

	// Set child to runnable last.
	rc = sys_env_set_status(envid_child, ENV_RUNNABLE);
	if (rc < 0) {
		return rc;
	}

	return envid_child;
	
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
