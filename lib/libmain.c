// Called from entry.S to get us going.
// entry.S already took care of defining envs, pages, uvpd, and uvpt.

#include <inc/lib.h>

extern void umain(int argc, char **argv);

const volatile struct Env *thisenv;
const char *binaryname = "<unknown>";

// IMPORTANT EXTERNAL DEFINITIONS //
//
// Env is defined in inc/env.h
// ENVX is defined in inc/env.h
// sys_getenvid() is defined in kern/syscall.c
// umain is described in inc/lib.h. I don't know where it is
// defined, if anywhere.
//
// envs is defined in kern/env.c. It's in context here because
// entry.S set it up that way.
//
// inc/env.h is included via inc/lib.h
// inc/syscall.h is included via inc/lib.h
//
// IMPORTANT EXTERNAL DEFINITIONS //

void
libmain(int argc, char **argv)
{

	// set thisenv to point at our Env structure in envs[].
	// LAB 3: Your code here.
	//
	// ENVX calculates this environment's offset in the envs arrary
	// based on this environment's ID.
	thisenv = &envs[ENVX(sys_getenvid())];

	// save the name of the program so that panic() can use it
	if (argc > 0)
		binaryname = argv[0];

	// call user main routine
	umain(argc, argv);

	// exit gracefully
	exit();

}
