// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/assert.h>
#include <inc/memlayout.h>
#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/dwarf.h>
#include <kern/dwarf_api.h>
#include <kern/kdebug.h>
#include <kern/monitor.h>
#include <kern/pmap.h>
#include <kern/trap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line


struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands.", mon_help },
	{
		"kerninfo",
		"Display information about the kernel.",
		mon_kerninfo
	},
	{
		"backtrace",
		"A report of the active stack frames.",
		mon_backtrace
	},
	{
		"showmappings",
		"Display the physical page mappings between two\n" \
		"virtual addresses.",
		mon_showmappings
	},
};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
/*
 *  A report of the active stack frames.
 *
 *  Parameters
 *  ----------
 *  Parameters currently do nothing. Passing anything as arguments
 *  will return the same printout, which is a report of the active
 *  stack frames.
 *
 *  Returns
 *  -------
 *  0 : Printout was successful.
 *  	Currently, the mon_backtrace cannot detect if the printout
 *  	failed. Therefore, it always returns 0.
 *
 */
{

	uint64_t arg, cfa, rbp, rip;
	
	cprintf("Stack backtrace:\n");

	rbp = read_rbp();
	read_rip(rip);

	// The kernal puts NULL as a sentinel on the bottom of the
	// stack.
	while (rbp) {

		// zero out info.
		// Otherwise DWARF failures may not be noticed.
		struct Ripdebuginfo info = {0};

		/* 
		 * Negative return from debuginfo_rip indicates
		 * that not all information could be found.
		 * However DWARF still includes in info what it
		 * is able to find. It does not give a way to
		 * determine what was found and what was not.
		 */
		if (debuginfo_rip(rip, &info) < 0) {
			cprintf(
				"Not all DWARF info could be " \
				"found.\nZero may indicate DWARF " \
				"failure rather than the value " \
				"indicated.\n"
			);
		}
		// cprintf returns the number of characters printed.
		cprintf(
			"rbp %016llx rip %016llx\n%s:%d: " \
			"%s+%016llx args:%d ",
			rbp, rip,
			info.rip_file, info.rip_line,
			info.rip_fn_name, rip - info.rip_fn_addr,
			info.rip_fn_narg
		);
		// cfa's offset from rbp is stored in the following
		// location in info struct;
		cfa = rbp + info.reg_table.cfa_rule.dw_offset;

		// For multiple arguments, argument precedence is
		// indicated by higher memory addresses on the stack.
		size_t i;
		for (i = info.rip_fn_narg; i > 0; --i) {

			arg = *(uint64_t*)(
				cfa + info.offset_fn_arg[i-1]
			);

			/*
 			 * arg is 64 bits, but there are 32 bit values
 			 * on the stack. If a 32 bit value is put
 			 * into arg, then the higher 32 bits can have
 			 * garbage values. The whole 64 bits are not
 			 * overwritten.
 			 * This bitshifting erases any high bits that
 			 * may not be overwritten.
 			 * The size of the argument is stored in
 			 * info.size_fn_arg.
			 */
			arg = (arg << (
				(8 * sizeof(arg)) - (8 * info.size_fn_arg[i-1]))
			) >> info.size_fn_arg[i-1] * 8;	

			if (i > 1) {
				cprintf("%016llx ", arg);
			} else {
				cprintf("%016llx\n", arg);
			}

		}

		// Prior rip is value of the address one higher
		// than current rbp.
		rip = *(uint64_t *)(rbp + sizeof(rbp));
		// Prior rbp is stored as the value at the
		// current rbp address 
		rbp = *(uint64_t *)rbp;

	}

	return 0;

}

int
mon_showmappings(int argc, char **argv, struct Trapframe *tf)
/*
 *  Prints out all physical page mappings and associated
 *  permission bits for a range of virtual addresses.
 *
 *  Parameters
 *  ----------
 *  start	: The starting virtual address of the desired
 *  		  printout range, in hexadecimal. If not page
 *  		  aligned, this value will be rounded down in order
 *  		  to be page aligned.
 *  end		: The ending virtual address of the desired printout
 *  		  range, in hexadecimal. The page associated with
 *  		  the ending address will be printed. If not page
 *  		  aligned, this value will be rounded down in order
 *  		  to be page aligned.
 *  key_bool	: Optional parameter. If passed and a '0', the key
 *  		  will not be printed. If passed and not '0', this
 *  		  value is silently ignored.
 *
 *  Returns
 *  -------
 *  -2		: -2 is returned on illegally formatted bounds.
 *  -1		: -1 is returned if less than 2 arguments are
 * 		  provided.
 *  0		: 0 is returned on success.
 *
 *  Notes
 *  ----- 
 *  The first parameter in argc is start, the second parameter is
 *  end, and the optional third parameter is key_bool. Any excess
 *  parameters are silently ignored.
 *
 *  Any Trapframe passed is silently ignored.
 *
 */
{

	if (argc < 3) {
		cprintf(
			"showmappings illegal input: Need bounds, " \
			"from lower bound to upper bound, of\n" \
			"hexadecimal virtual addresses to show " \
			"mappings for.\n"
		);
		return -1;
	}

	uint64_t end;
	char *endp = NULL;
	char* illegal_input = (
		"showmappings illegal input: %s is not a virtual\n" \
		"address in hexadecimal.\n"
	);
	char *key;
	int key_bool = 1;
	uint64_t start;

	if (argc == 4 && argv[3][0] == '0') {
		key_bool = 0;
	}
	// strol implemented in lib/string.c does not properly detect
	// overflow.
	// There's actually a comment in the file stating
	// that fact directly.
	start = strtol(argv[1], &endp, 16);
	if (endp != argv[1] + strlen(argv[1]) || start < 0) {
		cprintf(illegal_input, argv[1]);
		return -2;
	}
	end = strtol(argv[2], &endp, 16);
	if (endp != argv[2] + strlen(argv[2]) || end < 0) {
		cprintf(illegal_input, argv[2]);
		return -2;
	}
	ROUNDDOWN(start, PGSIZE);
	ROUNDDOWN(end, PGSIZE);
	if (end < start) {
		cprintf(
			"showmappings illegal input: end bound " \
			"is not greater than the start bound.\n"
		);
		return -2;
	}

	if (key_bool) {
		key = (
			"        VA        |        PA        |NX|G|" \
			"PAT|D|A|PCD|PWT|U|W|P|\n"
		);
		cprintf(
			"For an explanation of the acronyms used in the " \
			"key, see:\nhttp://cs.unc.edu/~porter/courses/" \
			"comp530/f18/ref/amd64/" \
			"AMD64_Architecture_Programmers_Manual.pdf\n\n"
		);
	}

	pml4e_t *cr3 = KADDR(rcr3());
	pte_t *pte;
	void *va;
    size_t i;
	for (i = 0; i <= end - start; i += PGSIZE) {

		// If printing the key, do so every 40 lines. That
		// way long outputs always have the key around to
		// reference.
		if (key_bool && i % 0x28000 == 0) {
			cprintf(key);
		}	
		va = (void *)start + i;
		// rcr3 is defined in inc/x86.h
		// rcr3 has no failure indication.
		pte = pml4e_walk(
			cr3, va, 0
		);	
		if (!pte || !(*pte & PTE_P)) {
			cprintf(
				"0x%016x| NOT PHYSICALLY MAPPED\n",
				va
			);
		} else {
			cprintf(
				"0x%016x|0x%016x| %d|%d|" \
				" %d |%d|%d| %d | %d |" \
				"%d|%d|%d|\n",
				va, (PTE_ADDR(*pte)),
				*pte >> 63,
				(*pte & 0x100) >> 8,
				(*pte & PTE_PS) >> 7,
				(*pte & PTE_D) >> 6,
				(*pte & PTE_A) >> 5,
				(*pte & PTE_PCD) >> 4,
				(*pte & PTE_PWT) >> 3,
				(*pte & PTE_U) >> 2,
				(*pte & PTE_W) >> 1,
				(*pte & PTE_P)
			);
		}

	}

	return 0;

}


/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
