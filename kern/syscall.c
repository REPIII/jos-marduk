/* See COPYRIGHT for copyright information. */

#include <inc/assert.h>
#include <inc/error.h>
#include <inc/string.h>
#include <inc/types.h>
#include <inc/x86.h>

#include <kern/common.h>
#include <kern/console.h>
#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/sched.h>
#include <kern/syscall.h>
#include <kern/trap.h>

// IMPORTANT EXTERNAL DESCRIPTIONS //
//
// cons_getc is defined in kern/console.c
// curenv is defined in kern/env.h
// DEBUG is defined in kern/commonh.
// envid2env is defined in kern/env.c
// env_alloc is defined in kern/env.c
// env_destroy is defined in kern/env.c
// page_alloc is defined in kern/pmap.c
// page_insert is defined in kern/pmap.c
// page_lookup is defined in kern/pmap.c
// page_remove is defined in kern/pmap.c
// ROUDNDOWN is defined in inc/types.c
// Trapframe is defined in inc/trap.c
// user_mem_assert is defined in kern/pmap.c
// user_mem_check is defined in kern/pmap.c
// UTOP is defined in inc/memlayout.h
//
// All error codes are defined in inc/error.h
// Note that error codes do not lead with "-". That is to make the
// positive enum value negative, indicating an error.
//
// All PTE bits are defined in inc/mmu.h
//
// All syscall numbers are defined in inc/syscall.h through
// an enum.
//
// IMPORTANT EXTERNAL DESCRIPTIONS //

// Print a string to the system console.
// The string is exactly 'len' characters long.
// Destroys the environment on memory errors.
static void
sys_cputs(const char *s, size_t len)
{
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.
	// LAB 3: Your code here.
	//
	// user_mem_assert checks input s for us.
	// Don't bother passing PTE_P. user_mem_assert does it for you.
	// user_mem_assert has no failure indication. It destroys the
	// environment instead.
	user_mem_assert(curenv, (const void*)s, len, PTE_U);

	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
}

// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
}

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
}

// Destroy a given environment (possibly the currently running environment).
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//	or the caller doesn't have permission to change envid.
static int
sys_env_destroy(envid_t envid)
{

	// From Lab 5 skeleton.
	//
	//int r;
	//struct Env *e;
	//
	//if ((r = envid2env(envid, &e, 1)) < 0)
	//	return r;
	//env_destroy(e);
	//return 0;

	// From Lab 3 skeleton.
	//
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
		return r;

    #if DEBUG
	if (e == curenv)
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);

    #endif // #if DEBUG
	env_destroy(e);
	return 0;
}

// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{
	sched_yield();
}

// Allocate a new environment.
// Returns envid of new environment, or < 0 on error.  Errors are:
//	-E_NO_FREE_ENV if no free environment is available.
//	-E_NO_MEM on memory exhaustion.
static envid_t
sys_exofork(void)
{

	// Create the new environment with env_alloc(), from kern/env.c.
	// It should be left as env_alloc created it, except that
	// status is set to ENV_NOT_RUNNABLE, and the register set is copied
	// from the current environment -- but tweaked so sys_exofork
	// will appear to return 0.
	//
	// LAB 4: Your code here.

	if (!curenv) {
		panic(
			"sys_exofork panic: No parent environemnt to fork " \
			"from. curenv is NULL."
		);	
	}

	struct Env *env_child = NULL;

	// We care why env_alloc failed, as we return that code as well.
	// Thus, save it and return it on failure.
	int rc = env_alloc(&env_child, curenv->env_id);
	if (!env_child) {

		return -E_NO_FREE_ENV;

	} else if (rc < 0) {

		return rc;

	} else {

		env_child->env_status = ENV_NOT_RUNNABLE;
		env_child->env_tf = curenv->env_tf;

		// Make child return 0;
		env_child->env_tf.tf_regs.reg_rax = 0;

		return env_child->env_id;

	}

}

// Set envid's env_status to status, which must be ENV_RUNNABLE
// or ENV_NOT_RUNNABLE.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if status is not a valid status for an environment.
static int
sys_env_set_status(envid_t envid, int status)
{
	// Hint: Use the 'envid2env' function from kern/env.c to translate an
	// envid to a struct Env.
	// You should set envid2env's third argument to 1, which will
	// check whether the current environment has permission to set
	// envid's status.
	//
	// LAB 4: Your code here.

	struct Env *env = NULL;

	if (envid2env(envid, &env, 1) < 0 || !env) {
		return -E_BAD_ENV;
	} else if (
		status != ENV_RUNNABLE
		&& status != ENV_NOT_RUNNABLE
	) {
		return -E_INVAL;
	}

	env->env_status = status;

	return 0;

}

// Set envid's trap frame to 'tf'.
// tf is modified to make sure that user environments always run at code
// protection level 3 (CPL 3) with interrupts enabled.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	// LAB 5: Your code here.
	// Remember to check whether the user has supplied us with a good
	// address!

	struct Env *env = NULL;

	if (
		!tf
		|| user_mem_check(
			curenv,
			tf,
			sizeof(struct Trapframe),
			PTE_P | PTE_W | PTE_U
		) < 0
	) {
		return -E_INVAL;
	} else if ((envid2env(envid, &env, 1) < 0) || !env) {
		return -E_BAD_ENV;
	}

	env->env_tf = *tf;
	//Run at protection level 3.
	env->env_tf.tf_cs = GD_UT | 3;
	// Enable interrupts.
	env->env_tf.tf_eflags = FL_IF;

	return 0;

}

// Set the page fault upcall for 'envid' by modifying the
// corresponding struct Env's 'env_pgfault_upcall' field.
// When 'envid' causes a page fault, the kernel will push a fault
// record onto the exception stack, then branch to 'func'.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int
sys_env_set_pgfault_upcall(envid_t envid, void *func)
{
	// LAB 4: Your code here.

	struct Env *env = NULL;

	if (!func) {
		return -E_INVAL;
	} else if ((envid2env(envid, &env, 1) < 0) || !env) {
		return -E_BAD_ENV;
	} else {
		env->env_pgfault_upcall = func;
		return 0;
	}

}

// Allocate a page of memory and map it at 'va' with permission
// 'perm' in the address space of 'envid'.
// The page's contents are set to 0.
// If a page is already mapped at 'va', that page is unmapped as a
// side effect.
//
// perm -- PTE_U | PTE_P must be set, PTE_AVAIL | PTE_W may or may not be set,
//         but no other bits may be set.  See PTE_SYSCALL in inc/mmu.h.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
//	-E_INVAL if perm is inappropriate (see above).
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	// Hint: This function is a wrapper around page_alloc() and
	//   page_insert() from kern/pmap.c.
	//   Most of the new code you write should be to check the
	//   parameters for correctness.
	//   If page_insert() fails, remember to free the page you
	//   allocated!
	//
	// LAB 4: Your code here.

	// PTE_SHARE == 0x400
	//if (perm & 0x400) {
	//	cprintf("ALLOCATING SHARED PAGE va: %p PERM: %x\n", va, perm);
	//}

	struct Env *env = NULL;

	// envid2env returns 0 on success.
	// We don't care why it failed if it did.
	if (envid2env(envid, &env, 1) < 0 || !env) {
		return -E_BAD_ENV;
	} else if (
		va >= (void*)UTOP
		|| ROUNDDOWN(va, PGSIZE) != va
		|| (perm | PTE_SYSCALL) != PTE_SYSCALL
		|| ((perm & (PTE_P | PTE_U)) != (PTE_P | PTE_U))
	) {
		return -E_INVAL;
	}

	struct PageInfo *pp = page_alloc(ALLOC_ZERO);
	if (!pp) {
		return -E_NO_MEM;
	}

	// page_insert returns 0 on sucess.
	// We don't care why it failed if it did.
	if (
		page_insert(
			env->env_pml4e, pp, va, perm
		) < 0
	) {
		// page_free has no failure indication.
		page_free(pp);
		return -E_NO_MEM;
	}

	return 0;

}

// Map the page of memory at 'srcva' in srcenvid's address space
// at 'dstva' in dstenvid's address space with permission 'perm'.
// Perm has the same restrictions as in sys_page_alloc, except
// that it also must not grant write access to a read-only
// page.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if srcenvid and/or dstenvid doesn't currently exist,
//		or the caller doesn't have permission to change one of them.
//	-E_INVAL if srcva >= UTOP or srcva is not page-aligned,
//		or dstva >= UTOP or dstva is not page-aligned.
//	-E_INVAL is srcva is not mapped in srcenvid's address space.
//	-E_INVAL if perm is inappropriate (see sys_page_alloc).
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate any necessary page tables.
static int
sys_page_map(envid_t envid_src, void *va_src,
	     envid_t envid_dst, void *va_dst, int perm)
{
	// Hint: This function is a wrapper around page_lookup() and
	//   page_insert() from kern/pmap.c.
	//   Again, most of the new code you write should be to check the
	//   parameters for correctness.
	//   Use the third argument to page_lookup() to
	//   check the current permissions on the page.
	//
	// LAB 4: Your code here.

	struct Env *env_src = NULL;
	struct Env *env_dst = NULL;

	if (
		envid2env(envid_src, &env_src, 1) < 0
		|| envid2env(envid_dst, &env_dst, 1) < 0
		|| !env_src || !env_dst
	) {
		return -E_BAD_ENV;
	} else if (
		va_src >= (void*)UTOP 
		|| ROUNDDOWN(va_src, PGSIZE) != va_src
		|| va_dst >= (void*)UTOP
		|| ROUNDDOWN(va_dst, PGSIZE) != va_dst
		|| (perm | PTE_SYSCALL) != PTE_SYSCALL
		|| (perm & (PTE_U | PTE_P)) != (PTE_U | PTE_P)
	){
		return -E_INVAL;
	}

	// pte MUST be NULL for page_lookup to work.
	pte_t *pte = NULL;
	struct PageInfo *page_src = page_lookup(
		env_src->env_pml4e, va_src, &pte
	); 

	if (
		!page_src || !pte
		|| ((((*pte) & PTE_W) != PTE_W) && ((perm & PTE_W) == PTE_W))
	) {
		return -E_INVAL;
	} else if (
		page_insert(
			env_dst->env_pml4e, page_src, va_dst, perm
		) < 0
	) {
		return -E_NO_MEM;
	}

	return 0;

}

// Unmap the page of memory at 'va' in the address space of 'envid'.
// If no page is mapped, the function silently succeeds.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
static int
sys_page_unmap(envid_t envid, void *va)
{
	// Hint: This function is a wrapper around page_remove().
	//
	// LAB 4: Your code here.

	struct Env *env = NULL;

	if (envid2env(envid, &env, 1) < 0 || !env) {
		return -E_BAD_ENV;
	} else if (va >= (void*)UTOP || ROUNDDOWN(va, PGSIZE) != va) {
		return -E_INVAL;
	}

	// page_remove has no failure indication.
	page_remove(env->env_pml4e, va);

	return 0;

}

// Try to send 'value' to the target env 'envid'.
// If srcva < UTOP, then also send page currently mapped at 'srcva',
// so that receiver gets a duplicate mapping of the same page.
//
// The send fails with a return value of -E_IPC_NOT_RECV if the
// target is not blocked, waiting for an IPC.
//
// The send also can fail for the other reasons listed below.
//
// Otherwise, the send succeeds, and the target's ipc fields are
// updated as follows:
//    env_ipc_recving is set to 0 to block future sends;
//    env_ipc_from is set to the sending envid;
//    env_ipc_value is set to the 'value' parameter;
//    env_ipc_perm is set to 'perm' if a page was transferred, 0 otherwise.
// The target environment is marked runnable again, returning 0
// from the paused sys_ipc_recv system call.  (Hint: does the
// sys_ipc_recv function ever actually return?)
//
// If the sender wants to send a page but the receiver isn't asking for one,
// then no page mapping is transferred, but no error occurs.
// The ipc only happens when no errors occur.
//
// When the environment is a guest (Lab 8, aka the VMM assignment only),
// srcva should be assumed to be converted to a host virtual address (in
// the kernel address range).  You will need to add a special case to allow
// accesses from ENV_TYPE_GUEST when srcva > UTOP.
//
// Returns 0 on success, < 0 on error.
// Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist.
//		(No need to check permissions.)
//	-E_IPC_NOT_RECV if envid is not currently blocked in sys_ipc_recv,
//		or another environment managed to send first.
//	-E_INVAL if srcva < UTOP but srcva is not page-aligned.
//	-E_INVAL if srcva < UTOP and perm is inappropriate
//		(see sys_page_alloc).
//	-E_INVAL if srcva < UTOP but srcva is not mapped in the caller's
//		address space.
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in the
//		current environment's address space.
//	-E_NO_MEM if there's not enough memory to map srcva in envid's
//		address space.
static int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm)
{

	// srcva can be NULL or 0.
	// Do not check for it.

	struct Env *env = NULL;
	struct PageInfo *pp = NULL;
	// pte MUST be set to NULL for page_lookup to work.
	pte_t *pte = NULL;

	// Checking whether envid is a valid environemnt ID.
	// Don't check permissions with envid2env.
	if (
		envid2env(envid, &env, 0) < 0
		|| !env
	) {
		return -E_BAD_ENV;
	// Check if the destination enviroment is recieving.
	} else if (!(env->env_ipc_recving)) {
		return -E_IPC_NOT_RECV;
	// Check if we want to give the destination environment a page,
	// and that it is in user space.
	} else if (srcva < (void *)UTOP) {
		// Check that the page is page aligned.
		if (ROUNDDOWN(srcva, PGSIZE) != srcva) {
			return -E_INVAL;
		}
		// Check if we have mapped that page.
		pp = page_lookup(curenv->env_pml4e, srcva, &pte);
		if (!pp) {
			return -E_INVAL;
		// Check if the permissions requested are valid.
		} else if (
			(perm | PTE_SYSCALL) != PTE_SYSCALL
			|| ((perm & (PTE_P | PTE_U)) != (PTE_P | PTE_U))
		) {
			return -E_INVAL;
		// Ensure we don't give a read-only page over as writeable.
		} else if (!pte || ((perm & PTE_W) && ((*pte & PTE_W) == 0))) {
			return -E_INVAL;
		}
	}

	// Only insert if the destination process gave us an address
	// to insert.
	// If no insertion occurs, env_ipc_perm is set to zero.
	if (pp && env->env_ipc_dstva < (void *)UTOP) {

		if (page_insert(
			env->env_pml4e, pp, env->env_ipc_dstva, perm
		) < 0) {
			return -E_NO_MEM;
		} else {
			env->env_ipc_perm = perm;
		}

	} else {
		env->env_ipc_perm = 0;
	}

	env->env_ipc_recving = 0;
	env->env_ipc_dstva = (void *)-1;
	env->env_ipc_value = value;
	env->env_ipc_from = curenv->env_id;

	// The destination enviroment returns 0 from sys_ipc_recv.
	// We have to set this since sys_ipc_recv never returns.
	env->env_tf.tf_regs.reg_rax = 0;

	env->env_status = ENV_RUNNABLE;

	return 0;

}

// Block until a value is ready.  Record that you want to receive
// using the env_ipc_recving and env_ipc_dstva fields of struct Env,
// mark yourself not runnable, and then give up the CPU.
//
// If 'dstva' is < UTOP, then you are willing to receive a page of data.
// 'dstva' is the virtual address at which the sent page should be mapped.
//
// This function only returns on error, but the system call will eventually
// return 0 on success.
// Return < 0 on error.  Errors are:
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
static int
sys_ipc_recv(void *dstva)
{

	// dstva can be NULL or 0.
	// Do not check for it.

	if (
		dstva < (void *)UTOP
		&& ROUNDDOWN(dstva, PGSIZE) != dstva
	) {
		return -E_INVAL;
	}

	curenv->env_ipc_recving= 1;
	if (dstva < (void *)UTOP) {
		   curenv->env_ipc_dstva = dstva;
	} else {
		   curenv->env_ipc_dstva = (void *)-1;
	}

	// Zero out old values as best we can.
	// Not necessary, sys_ipc_try_send should handle this
	// appropriately, but let's be safe for now.
	curenv->env_ipc_value = 0;
	curenv->env_ipc_from = 0;
	curenv->env_ipc_perm = 0;

	curenv->env_status = ENV_NOT_RUNNABLE;
	sched_yield();

	// sched_yield never return, but this makes the compiler happy.
	return 0;

}


// Dispatches to the correct kernel function, passing the arguments.
int64_t
syscall(uint64_t syscallno, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5)
{
	// Call the function corresponding to the 'syscallno' parameter.
	// Return any appropriate return value.
	// If the syscallno is not registered, return -E_NO_SYS.
	// LAB 3 & 4: Your code here

	switch (syscallno) {

		case (SYS_cputs):
			// sys_cputs has no failure indication. It destroys the
			// environment instead. Thus, always return 0.
			sys_cputs((const char *)a1, a2);
			return 0;
		case (SYS_cgetc):
			return sys_cgetc();
		case (SYS_getenvid):
			return sys_getenvid();
		case (SYS_env_destroy):
			return sys_env_destroy((envid_t)a1);
		case (SYS_page_alloc):
			return sys_page_alloc((envid_t)a1, (void *)a2, (int)a3);
		case (SYS_page_map):
			return sys_page_map(
					(envid_t)a1,
					(void *)a2,
					(envid_t)a3,
					(void *)a4,
					(int)a5
				);
		case (SYS_page_unmap):
			return sys_page_unmap((envid_t)a1, (void*)a2);
		case (SYS_exofork):
			return sys_exofork();
		case (SYS_env_set_status):
			return sys_env_set_status((envid_t)a1, (int)a2);
		case (SYS_env_set_trapframe):
			return sys_env_set_trapframe(
				(envid_t)a1, (struct Trapframe *)a2
			);
		case (SYS_env_set_pgfault_upcall):
			return sys_env_set_pgfault_upcall(
				(envid_t)a1, (void *)a2
			);
		case (SYS_yield):
			// sys_yield has no failure indication and never
			// returns.
			sys_yield();
			return 0;
		case (SYS_ipc_try_send):
			return sys_ipc_try_send(
				(envid_t)a1, (uint32_t)a2, (void *)a3, (unsigned) a4
			);
		case (SYS_ipc_recv):
			return sys_ipc_recv((void *)a1);
		default:
			return -E_NO_SYS;

	}

}
