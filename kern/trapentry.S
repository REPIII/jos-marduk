/* See COPYRIGHT for copyright information. */

#include <inc/memlayout.h>
#include <inc/mmu.h>
#include <inc/trap.h>

#include <kern/macro.h>
#include <kern/picirq.h>


###################################################################
# exceptions/interrupts
###################################################################

/* TRAPHANDLER defines a globally-visible function for handling a trap.
 * It pushes a trap number onto the stack, then jumps to _alltraps.
 * Use TRAPHANDLER for traps where the CPU automatically pushes an error code.
 *
 * You shouldn't call a TRAPHANDLER function from C, but you may
 * need to _declare_ one in C (for instance, to get a function pointer
 * during IDT setup).  You can declare the function with
 *   void NAME();
 * where NAME is the argument passed to TRAPHANDLER.
 *
 * Don't include a second pushq to match NOEC. x86 does that
 * for you.
 */
#define TRAPHANDLER(name, num)						\
	.globl name;		/* define global symbol for 'name' */	\
	.type name, @function;	/* symbol type is function */		\
	.align 2;		/* align function definition */		\
	name:			/* function starts here */		\
	pushq $(num);							\
	jmp _alltraps

/* Use TRAPHANDLER_NOEC for traps where the CPU doesn't push an error code.
 * It pushes a 0 in place of the error code, so the trap frame has the same
 * format in either case.
 */
#define TRAPHANDLER_NOEC(name, num)					\
	.globl name;							\
	.type name, @function;						\
	.align 2;							\
	name:								\
	pushq $0;							\
	pushq $(num);							\
	jmp _alltraps

.text

/*
 * Lab 3: Your code here for generating entry points for the different traps.
   Find the PUSHA,POPA macros in kern/macro.h. Since amd64 doesn't support
   pusha,popa so you will find these macros handy.
 */

	# Codes 0 - 9 have no error codes, except for 8.
	TRAPHANDLER_NOEC(xtrpx_divide, T_DIVIDE)
	TRAPHANDLER_NOEC(xtrpx_debug, T_DEBUG)
	TRAPHANDLER_NOEC(xtrpx_nmi, T_NMI)
	TRAPHANDLER_NOEC(xtrpx_brkpt, T_BRKPT)
	TRAPHANDLER_NOEC(xtrpx_oflow, T_OFLOW)
	TRAPHANDLER_NOEC(xtrpx_bound, T_BOUND)
	TRAPHANDLER_NOEC(xtrpx_illop, T_ILLOP)
	# System error, T_DBLFLT = 8, does have an error code.
	# It's always 0 though, and TRAPHANDLER_NOEC passes zero.
	TRAPHANDLER_NOEC(xtrpx_dblflt, T_DBLFLT)
	TRAPHANDLER_NOEC(xtrpx_coproc, T_COPROC)

	# Codes 10 - 14 have error codes.
	TRAPHANDLER(xtrpx_tss, T_TSS)
	TRAPHANDLER(xtrpx_segnp, T_SEGNP)
	TRAPHANDLER(xtrpx_stack, T_STACK)
	TRAPHANDLER(xtrpx_gpflt, T_GPFLT)
	TRAPHANDLER(xtrpx_pgflt, T_PGFLT)
	# Code 15 is reseved.
	# Code 16 has no error code.
	TRAPHANDLER_NOEC(xtrpx_fperr, T_FPERR)

	# FIXME Codes 17-19 are defined in JOS, but not in the given
	# 80386 manual. Thus, I don't know if they have error codes.
	TRAPHANDLER_NOEC(xtrpx_align, T_ALIGN)
	TRAPHANDLER_NOEC(xtrpx_mchk, T_MCHK)
	TRAPHANDLER_NOEC(xtrpx_simderr, T_SIMDERR)

	# Codes 48 and 500 are defined by JOS
	TRAPHANDLER_NOEC(xtrpx_syscall, T_SYSCALL)
	TRAPHANDLER_NOEC(xtrpx_default, T_DEFAULT)

	# All IRQ codes have no error codes.
	TRAPHANDLER_NOEC(xtrpx_irq_timer, IRQ_OFFSET + IRQ_TIMER)
	TRAPHANDLER_NOEC(xtrpx_irq_kbd, IRQ_OFFSET + IRQ_KBD)
	TRAPHANDLER_NOEC(xtrpx_irq_serial, IRQ_OFFSET + IRQ_SERIAL)
	TRAPHANDLER_NOEC(xtrpx_irq_spurious, IRQ_OFFSET + IRQ_SPURIOUS)
	TRAPHANDLER_NOEC(xtrpx_irq_ide, IRQ_OFFSET + IRQ_IDE)
	TRAPHANDLER_NOEC(xtrpx_irq_error, IRQ_OFFSET + IRQ_ERROR)

/*
 * Lab 3: Your code here for _alltraps
 *
 * Hint: Be sure to review the x64 calling convention from lab1
 *       for how to pass a pointer to the trapframe.
 */
_alltraps:

	# x86 PUSHES SS, RSP, EFLAGS, CS, RIP, AND ERROR_CODE
	# YOU DON'T HAVE TO!
	# trapno is pushed by the specific traphandlers above.
	#
	# r10 is our general use register of this function.

	movq $0, %r10			# Zero out r10.
	movw %ds, %r10w			# Set r10 = ds.
	pushq %r10				# Push ds.
	movw %es, %r10w			# Set r10 = es.
	pushq %r10				# Push es.
	PUSHA					# Push all general purpose registers.

	movw $(GD_KD), %r10w	# Set r10 = $(GD_KD)
	movw %r10w, %ds			# Set ds = $(GD_KD)
	movw %r10w, %es			# Set es = $(GD_KD)

	movq %rsp, %rdi			# Set first argument, rdi, to the stack
							# pointer.
	call trap				# Call trap

	POPA_					# Pop all general purpose registers.
							# Yes it must be POPA_, not POPA.
	popq %r10				# Pop es. No need to zero out r10
							# since it's a 64-bit pop.
	movw %r10w, %es			# Set es = r10.
	popq %r10				# Pop ds.
	movw %r10w, %ds			# Set ds = r10.
	addq $16, %rsp			# Decrement stack pointer for trap_no
							# and err_no pushed by the handlers
							# above.

	iret					# Return.
