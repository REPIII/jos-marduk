/* See COPYRIGHT for copyright information. */

#ifndef JOS_KERN_TRAP_H
#define JOS_KERN_TRAP_H
#ifndef JOS_KERNEL
# error "This is a JOS kernel header; user programs should not #include it"
#endif

#include <inc/trap.h>
#include <inc/mmu.h>

/* The kernel's interrupt descriptor table */
extern struct Gatedesc idt[];
extern struct Pseudodesc idt_pd;


// Trap handlers defined in kern/trapentry.S
// Prof Porter said they should return char. Not really sure why.
// xtrpx prefix suggested by lab instructions to avoid collisions.
char xtrpx_divide(void);
char xtrpx_debug(void);
char xtrpx_nmi(void);
char xtrpx_brkpt(void);
char xtrpx_oflow(void);
char xtrpx_bound(void);
char xtrpx_illop(void);
char xtrpx_dblflt(void);
char xtrpx_coproc(void);
char xtrpx_tss(void);
char xtrpx_segnp(void);
char xtrpx_stack(void);
char xtrpx_gpflt(void);
char xtrpx_pgflt(void);
char xtrpx_fperr(void);
char xtrpx_align(void);
char xtrpx_mchk(void);
char xtrpx_simderr(void);
char xtrpx_syscall(void);
char xtrpx_default(void);
char xtrpx_irq_timer(void);
char xtrpx_irq_kbd(void);
char xtrpx_irq_serial(void);
char xtrpx_irq_spurious(void);
char xtrpx_irq_ide(void);
char xtrpx_irq_error(void);


void trap_init(void);
void trap_init_percpu(void);
void print_regs(struct PushRegs *regs);
void print_trapframe(struct Trapframe *tf);
void page_fault_handler(struct Trapframe *);
void backtrace(struct Trapframe *);

#endif /* JOS_KERN_TRAP_H */
